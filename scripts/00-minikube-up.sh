#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

# check kubectl
command -v kubectl >/dev/null 2>&1 || {
    echo "${RED}Could not find kubectl on machine...${NC}";
    exit 101;
}

case "$(uname -s)" in
    # if running on MacOS then use hyperkit virtualizator
    Darwin*)
        minikube config set driver hyperkit
    ;;
    # if running on Windows on mingw then use hyperv virtualizator
    # Commenting this out as:
    #   1. Running minikube on Hyper-V is very time consuming
    #   2. minikube has issues with persistant volumes on Hyper-V
    MINGW*)
      minikube config set vm-driver hyperv
      HYPERV_OPTIONS=' --hyperv-use-external-switch';;
    *)
        # check virtualbox
        command -v virtualbox >/dev/null 2>&1 || {
            echo "${RED}Could not find virtualbox on machine...${NC}";
            exit 102;
        }
        minikube config set driver virtualbox;
    ;;
esac

( minikube status ) || minikube start --kubernetes-version "${KUBERNETES_VERSION}" ${HYPERV_OPTIONS} --memory "2gb" --cpus 2 --disk-size "20gb";


# enable metrics server
minikube addons enable metrics-server;

MINIKUBE_IP=$(minikube ip);


echo ;
echo "Cool! MiniKube is up on ${MINIKUBE_IP} address.";
