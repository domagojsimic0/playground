#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

# check if minikube is running
if ! minikube status > /dev/null; then
    echo -e "${RED}Minikube is not running. Please start minikube with 00-minikube-up.sh${NC}";
    exit 101;
fi

# check kubectl
command -v kubectl >/dev/null 2>&1 || {
    echo "${RED}Could not find kubectl on machine...${NC}";
    exit 102;
}

# check git
command -v helm >/dev/null 2>&1 || {
    echo "${RED}Could not find helm on machine...${NC}";
    exit 103;
}

# check git
command -v git >/dev/null 2>&1 || {
    echo "${RED}Could not find git on machine...${NC}";
    exit 104;
}

# clone git repo
git_repo_dir="$(mktemp -d -p "$DIR")"
if [[ ! "$git_repo_dir" || ! -d "$git_repo_dir" ]]; then
  echo "Could not create temp dir for cloning git repo"
  exit 105
fi
trap '{ rm -rf -- "$git_repo_dir"; }' EXIT
pushd "$git_repo_dir" > /dev/null
    git clone "$MANIFEST_GIT_REPO" .
popd > /dev/null

# create namespace
kubectl create namespace argocd --dry-run=client -o yaml | kubectl apply -f -
# add helm repo
helm repo add argo https://argoproj.github.io/argo-helm
# install helm chart
if helm status argocd --namespace argocd >/dev/null 2>&1; then
    helm upgrade --namespace argocd --set server.service.type=NodePort --set server.service.nodePortHttp=32600 argocd argo/argo-cd
else
    helm install --namespace argocd --set server.service.type=NodePort --set server.service.nodePortHttp=32600 argocd argo/argo-cd
fi

# command to patch argocd if it was install via k8s manifests
# kubectl patch svc argocd-argocd-server --patch  '{"spec": { "type": "NodePort", "ports": [ { "name": "http", "nodePort": 32600, "port": 80, "targetPort": "server" } ] } }'
# wait for the deployments
kubectl rollout status deployment argocd-application-controller -n argocd;
kubectl rollout status deployment argocd-dex-server -n argocd;
kubectl rollout status deployment argocd-redis -n argocd;
kubectl rollout status deployment argocd-repo-server -n argocd;
kubectl rollout status deployment argocd-server -n argocd;

# create argo projects
kubectl apply -n argocd -f "$git_repo_dir"/kubernetes/argocd/projects/project.yaml

#create argo applications
kubectl apply -n argocd -f "$git_repo_dir"/kubernetes/argocd/mainApplication.yaml

echo "Setup finished! Argocd is available on $(minikube ip):32600"

exit 0
