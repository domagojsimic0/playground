#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

# check if minikube is running
if ! minikube status > /dev/null; then
    echo -e "${RED}Minikube is not running. Please start minikube with 00-minikube-up.sh${NC}";
    exit 101;
fi

# check kubectl
command -v kubectl >/dev/null 2>&1 || {
    echo "${RED}Could not find kubectl on machine...${NC}";
    exit 102;
}

# check git
command -v git >/dev/null 2>&1 || {
    echo "${RED}Could not find git on machine...${NC}";
    exit 103;
}

# clone git repo
git_repo_dir="$(mktemp -d -p "$DIR")"
if [[ ! "$git_repo_dir" || ! -d "$git_repo_dir" ]]; then
  echo "Could not create temp dir for cloning git repo"
  exit 104
fi
trap '{ rm -rf -- "$git_repo_dir"; }' EXIT
pushd "$git_repo_dir" > /dev/null
    git clone "$MANIFEST_GIT_REPO" .
popd > /dev/null

# create namespaces
kubectl apply -f "$git_repo_dir"/kubernetes/apps/namespace.yaml

# create deployment
kubectl apply -n apps -f "$git_repo_dir"/kubernetes/apps/sportening/deployment.yaml

# wait for the deployments
kubectl rollout status deployment sportening-deployment -n apps;

# create services, autoscalers and distruption budgets
kubectl apply -n apps -f "$git_repo_dir"/kubernetes/apps/sportening/service.yaml \
                      -f "$git_repo_dir"/kubernetes/apps/sportening/horizontalPodAutoscaler.yaml \
                      -f "$git_repo_dir"/kubernetes/apps/sportening/podDisruptionBudget.yaml

echo "Setup finished! You app is available on $(minikube ip):32410"

exit 0
