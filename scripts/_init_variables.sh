#!/usr/bin/env bash

export KUBERNETES_VERSION="v1.23.1";
export MANIFEST_GIT_REPO="https://gitlab.com/domagojsimic0/gitops-node-deployments.git";
