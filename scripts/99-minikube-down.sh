#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

minikube delete;

echo ;
echo "MiniKube instance deleted.";
