#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

# check if minikube is running
if ! minikube status > /dev/null; then
    echo -e "${RED}Minikube is not running. Please start minikube with 00-minikube-up.sh${NC}";
    exit 101;
fi

# check kubectl
command -v kubectl >/dev/null 2>&1 || {
    echo "${RED}Could not find kubectl on machine...${NC}";
    exit 102;
}

# delete pdb
echo "Deleting sportening pod disruption budget...";
if kubectl get -n apps pdb sportening-pdb >/dev/null 2>&1; then
    kubectl delete -n apps pdb sportening-pdb
else
    echo "Sportening pod disruption budget doesn't exist";
fi

# delete hpa
echo "Deleting sportening horizontal pod autoscaler...";
if kubectl get -n apps hpa sportening-hpa >/dev/null 2>&1; then
    kubectl delete -n apps hpa sportening-hpa
else
    echo "Sportening horizontal pod autoscaler doesn't exist";
fi

# delete service
echo "Deleting sportening service...";
if kubectl get -n apps service sportening-service >/dev/null 2>&1; then
    kubectl delete -n apps service sportening-service
else
    echo "Sportening service doesn't exist";
fi

# delete deployment
echo "Deleting sportening deployment...";
if kubectl get -n apps deployment sportening-deployment >/dev/null 2>&1; then
    kubectl delete -n apps deployment sportening-deployment
else
    echo "Sportening deployment doesn't exist";
fi

echo "All resources deleted."

exit 0
