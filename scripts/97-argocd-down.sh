#!/usr/bin/env bash

set -e;

RED='\033[0;31m'
NC='\033[0m' # No Color

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# shellcheck source=./_init_variables.sh
source "${DIR}"/_init_variables.sh; \

# check minikube
command -v minikube >/dev/null 2>&1 || {
    echo -e "${RED}Could not find minikube on machine! Please install minikube...${NC}";
    exit 100;
}

# check if minikube is running
if ! minikube status > /dev/null; then
    echo -e "${RED}Minikube is not running. Please start minikube with 00-minikube-up.sh${NC}";
    exit 101;
fi

# check kubectl
command -v kubectl >/dev/null 2>&1 || {
    echo "${RED}Could not find kubectl on machine...${NC}";
    exit 102;
}

# delete application
echo "Deleting sportening application...";
if kubectl get -n argocd application sportening-main-app >/dev/null 2>&1; then
    kubectl delete -n argocd application sportening-main-app --wait=true
else
    echo "Sportening application doesn't exist";
fi

# delete application
echo "Deleting sportening project...";
if kubectl get -n argocd appproject sportening-project >/dev/null 2>&1; then
    kubectl delete -n argocd appproject sportening-project
else
    echo "Sportening project doesn't exist";
fi

# Delete argocd
echo "Uninstalling argocd helm chart...";
if helm status argocd -n argocd >/dev/null 2>&1; then
    helm uninstall argocd -n argocd
else
    echo "argocd chart doesn't exist";
fi

# Delete argocd
echo "Deleting argocd...";
if kubectl get namespace argocd >/dev/null 2>&1; then
    kubectl delete namespace argocd
else
    echo "argocd namespace doesn't exist";
fi

echo "Argocd deleted!"

exit 0
