const request = require('supertest')

describe('GET /', () => {
  it('respond with Hello Sportening!', (done) => {
    request('http://localhost:8000').get('/').expect('Hello Sportening!', done)
  })
})
