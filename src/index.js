// importing node framework
const express = require('express')

const app = express()

// Respond with "Hello Sportening!" for requests that hit our root "/"
app.get('/', function (req, res) {
  return res.send('Hello Sportening!')
})

// listen to port 8000 by default
app.listen(process.env.PORT || 8000, () => {
  console.log('Server is running')
})

module.exports = app
