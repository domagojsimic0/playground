# Task overview

This task consists of 3 parts of common DevOps related practices which one is dependent of previous part. Read carefully before you start implementing, as every part has it's hints to ease out your implementation.

## Part 1 - CI

Using this repository, your task is to dockerize the app (make it run in within a Docker container), 
and create CI/CD pipeline which is triggered on push to your default branch, with the following steps: 

1) Run lint
2) Run tests
3) If test succed, push the image to a Docker registry (of your choice) with a commit hash and "latest" as a Docker tag.

### Guidelines

This projects needs npm & node to run.
To install npm & node follow [this](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) link.

 * Install

```shell
npm install
```

 * Lint

```shell
npm run lint
```

 * Run

```shell
npm run start
```

App should be exposed on http://localhost:8000

Use CI tool of your choice. You can check which one you can use for free [here](https://www.g2.com/categories/continuous-integration/free)

Also there numerous free docker image hosting services, like [DockerHub](https://hub.docker.com/). You can create an account for free.

Hint: CI tool & Docker registry can be used for free on [GitLab](gitlab.com)

## Part 2 - CD

Goal of this task is to create scripts and resources enabling local Kubernetes & required services.

### Kubernetes

Deploy your newly created Docker application to Kubernetes cluster

* Deploy your app manifests to `apps` namespace

* Create a script `scripts/01-sportening-up.sh` following guidelines on the bottom of this manual

* Which workload should you use and why?

* Expose your application to 32410 port so that the application can be accessed via `http://$(minikube ip):32410`

* Which service can/should you use and why?

Hint: make your docker image repository public so you don't have to setup pull credentials on Kubernetes cluster

### GitOps

Use GitOps as continues deployment strategy for you newly created app.

* Create a new namespace called `argocd`

* Create a separate repository on your chosen repository manager called `gitops-node-deployments` which will store your k8s manifests

* Deploy ArgoCD following [this](https://argo-cd.readthedocs.io/en/stable/getting_started/) guide.

* (Bonus points) Deploy ArgoCD via Helm chart described [here](https://github.com/argoproj/argo-helm/tree/master/charts/argo-cd) instead of plain k8s manifests

* Expose ArgoCD to 32600 port so that the ArgoCD UI can be accessed via `http://$(minikube ip):32600`

* Configure ArgoCD application to listen on your newly created GitOps repository and automatically synchronize your k8s manifests on cluster if there are any changes

* Tweak your CI configuration and add a job called `gitops-deploy` which will update k8s manifests in `gitops-node-deployments` repository (check [updating-manifests](https://argo-cd.readthedocs.io/en/stable/user-guide/ci_automation/#update-the-local-manifests-using-your-preferred-templating-tool-and-push-the-changes-to-git)) for guidelines on how to configure this workflow

Hint: make your `gitops-node-deployments` repository public so you don't have to setup SSH keys/tokens for pulling projects on ArgoCD

If everything is set up correctly, you should see that when you push your changes to default branch, ArgoCD will automatically synchronize and deploy your new version of the app to k8s cluster.

### Guidelines and organisation

 * each written script should be verbose, and guide developer if something is missing
 * for ease of use, script prefix should start with two digits number, ie. `scripts/00-minikube-up.sh` (already provided)
 * each script should have it's undo (inverse) "sister" script that deletes stuff
 * inverse scripts should start with prefix 99, decreasing with each new script, ie. `scripts/99-minikube-down.sh` deletes minikube installation created with `scripts/00-minikube-up.sh`
 * all services installed on kubernetes should be exposed "outside" Kubernetes to host service as well

## Part 3 - SRE (Brainstorming on task review)

Think of how would you make your app highly available?

What can cause an applications donwtime on Kubernetes cluster?

How would you avoid those downtimes?
