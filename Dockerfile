FROM node:17.2.0-alpine3.14
RUN apk add dumb-init

ENV NODE_ENV production

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY package*.json ./
RUN npm install --production

COPY --chown=node:node . .

EXPOSE 8000

USER node
CMD [ "dumb-init", "node", "src/index.js" ]